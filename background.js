// TODO spending in last 7 days

chrome.runtime.onInstalled.addListener(function () {
  chrome.storage.local.get('dateInstalled', storage => {

    // being installed for the first time
    if (storage.dateInstalled === undefined) {
      const defaultSettings = {
        bankBalance: 500.00,
        bobaCost: 5.50,
        dateInstalled: new Date().getTime(),
        numBoba: 0,
        questionDelay: 18000000
      };
      chrome.storage.local.set(defaultSettings);
    }
  });

});

var timer = null;
// todo maybe use chrome.alarms?

chrome.runtime.onMessage.addListener(request => {

  console.log('background.js received a message: ' + request);
  if (request !== 'boba')
    return;
  chrome.storage.local.get(['questionDelay', 'triggerLock'], storage => {
    if (storage.triggerLock === undefined) {
      chrome.storage.local.set({ 'triggerLock': new Date().getTime() });
      timer = setTimeout(question, storage.questionDelay);
    }
  });
});

// clean up, clean up, everybody everywhere
chrome.runtime.onSuspend.addListener(function () {
  clearTimeout(timer);
});

chrome.runtime.onStartup.addListener(function () {
  chrome.storage.local.get('triggerLock', storage => {
    if (storage.triggerLock)
      question();
  });
});

function question() {
  chrome.storage.local.remove('triggerLock');
  // TODO change to a prettier modal that's injected into the page
  if (confirm('Did you get boba...\n"Cancel" for no, "OK" for yes')) {
    chrome.storage.local.get(['numBoba', 'bobaCost', 'bankBalance'], storage => {
      alert('Shame on you...');
      window.open('new_webpage/index.html', '_blank');
      // TODO talk about the average boba per week and per month since instllation
      chrome.storage.local.set(
        {
          numBoba: storage.numBoba + 1,
          bankBalance: storage.bankBalance - storage.bobaCost
        });
      // todo open shakming article boba unhealthy
    });
  }
  else {
    alert('good for you!');
  }
}
