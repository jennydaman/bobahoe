const input = document.getElementById('cost');
const bobaMonthly = document.getElementById('boba-per-month');
chrome.storage.local.get(['bobaCost', 'dateInstalled', 'numBoba'], data => {
  input.value = data.bobaCost;
  bobaMonthly.innerText = calculateCost(data.bobaCost, data.numBoba, data.dateInstalled);
});

function calculateCost(costPerBoba, numBoba, dateInstalled) {
  let monthsSinceInstall = (Date.now() - dateInstalled) / (1000 * 60 * 60 * 24);
  if (monthsSinceInstall < 1)
    monthsSinceInstall = 1;
  return (costPerBoba * numBoba / monthsSinceInstall).toFixed(2);
}
input.addEventListener('change', function (e) {
  const newCost = parseFloat(e.target.value);
  if (newCost !== '' && newCost > 0) {
    input.style.borderColor = "green";
    chrome.storage.local.set({ bobaCost: newCost });
    chrome.storage.local.get(['dateInstalled', 'numBoba'], data => {
      calculateCost(newCost, data.numBoba, data.dateInstalled);
    });
  }
  else
    input.style.borderColor = "red";
});
