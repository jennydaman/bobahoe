console.log('bobahoe is listening! '
          + 'Select "Verbose" in the Chromium console to see debugging messages.');

let message = '';
const TRIGGER_MATRIX = [
  ['i', 'we', "let's"],
  ['get', 'want'],
  ['bubble tea', 'boba']
];
const U_SRS = [
    ['go', 'meet', 'leav']
];
/**
 * 0: waiting for the initial request for boba
 * >0: number of messages after initial request for boba, waiting for
 * confirmation to get boba
 */
let state = 0;


document.addEventListener('keydown', keyLogger);

/**
 * Stores what the user is typing in the state variable `message`
 * 
 * @param {KeyboardEvent} e
 */
function keyLogger(e) {

  if (e.code === 'Backspace') {
    message = message.substring(0, message.length - 1);
    return;
  }
  if (e.code === 'Enter') {
    if (state === 0) {
      if (wantsBoba(message, TRIGGER_MATRIX))
        state = 1;
    }
    else {
      /*
       * try to detect if they are confirming that they want boba
       * by catching keywords in the next 5 messages.
       * if they do not confirm, then state is reset and we go back
       * to sniffing for requests for boba
       */
      if (wantsBoba(message, U_SRS)) {
        state = 0;
        hoeHandler();
      }
      else if (state++ > 5)
        state = 0;
    }

    if (wantsBoba(message, state === 0 ? TRIGGER_MATRIX : U_SRS))
      hoeHandler();
    message = '';
    console.debug('state: ' + state);
  }
  if (e.key.length > 1)
    return; // handles keys like Shift, PageDown
  message += e.key;

  console.debug(message);
}

/**
 * Checks if the String is a message about getting bubble tea.
 * Handles upper-case and lower-case.
 * 
 * Examples:
 * I want boba
 * I want bubble tea
 * I need bubble tea
 * Let's get boba
 * Let's get bubble tea
 * Let's go get boba
 * I WIlL LegIT dIE If i don'T geT bUBBLE tEA RighT NoW
 * 
 * @param {String} message
 * @param {String[][]} validMatrix
 */
function wantsBoba(message, validMatrix) {
    // TODO this function does not account for typos, nor is it efficient
  let current = 0;
  message = ' ' + message.trim().toLowerCase();
  
  let indexUnchecked = 0;
  while (indexUnchecked !== -1) {
    message = message.substring(indexUnchecked + 1);
    if (validMatrix[current].find(triggerWord => message.startsWith(triggerWord)) !== undefined) {
      if (++current >= validMatrix.length)
        return true;
    }
    indexUnchecked = message.indexOf(' ');
  }
  return false;
}

function hoeHandler() {
  chrome.storage.local.get(['bankBalance', 'bobaCost', 'triggerLock'], function(result) {
    if (result.triggerLock !== undefined) {
      console.debug('hoeHandler warning: triggerLock is defined');
      return;
    }
    alert(`By the way, you have ${money(result.bankBalance)} after a `
    + `${money(result.bobaCost)} boba you'll have ${money(result.bankBalance - result.bobaCost)}`);
  });
  chrome.runtime.sendMessage('boba');
}

function money(money) {
  return '$' + money.toFixed(2);
}
