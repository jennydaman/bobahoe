const manualBalance = document.getElementById('manual-balance');
const questDel = document.getElementById('question-delay');
chrome.storage.local.get(['bankBalance', 'questionDelay'], storage => {
  manualBalance.value = storage.bankBalance;
  questDel.value = storage.questionDelay;
});

manualBalance.addEventListener('change', e => {
  const newBalance = parseFloat(e.target.value);
  if (newBalance !== '' && newBalance > 0) 
    chrome.storage.local.set({bankBalance: newBalance});
});

questDel.addEventListener('change', e => {
  chrome.storage.local.set({questionDelay: parseFloat(e.target.value)});
});
// html form to change questionDelay
